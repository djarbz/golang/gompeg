/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitlab.com/djarbz/golang/gompeg/cmd"

func main() {
	cmd.Execute()
}
